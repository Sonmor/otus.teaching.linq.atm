﻿using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System.Linq;
using Sys = System.Console;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов

            var service = new ATMService(atmManager);

            Sys.WriteLine("1. Output user info:");
            Sys.WriteLine(service.GetUserInfo("lee", "222"));
            Sys.WriteLine(service.GetUserInfo("snow", "111"));

            Sys.WriteLine("2. Output account of user Snow info: ");
            Sys.WriteLine(service.GetUserAccountInfo("snow"));

            Sys.WriteLine("3. Output account of user Snow with history: ");
            Sys.WriteLine(service.GetAccountInfoWithHistory("snow"));

            Sys.WriteLine("4. Output info about all input operations: ");
            Sys.WriteLine(service.GetInfoAboutInputCashOperations());

            decimal max = 100000M;
            Sys.WriteLine($"5. Users how have cash more then {max}: ");
            Sys.WriteLine(service.GetUserSumm(max));

            Sys.WriteLine("Завершение работы приложения-банкомата...");
            Sys.ReadKey();
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}