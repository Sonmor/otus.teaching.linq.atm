﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services.Extensions;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMService
    {
        private readonly ATMManager manager;
        
        public ATMService(ATMManager manager)
        {
            this.manager = manager;
        }
        public bool IsUserExist(string login)
        {
            if (string.IsNullOrEmpty(login)) return false;
            return manager.Users.Any(x => login.Equals(x.Login));
        }
        public bool IsUserExist(User user)
        {
            if (user == null) return false;
            return IsUserExist(user.Login);
        }
        public User GetUser(string login)
        {
            if (!IsUserExist(login)) throw new ArgumentException(nameof(login)); 
            return manager.Users.First(x => login.Equals(x.Login));
        }

        public String GetUserInfo(string Login, string Passw)
        {
            if (!IsUserExist(Login)) return $"User \'{Login}\' not found";
            var user = GetUser(Login);

            if (!user.CheckPassword(Passw)) return "Incorrect password";
            return user.GetUserInfo();
        }
        public String GetUserAccountInfo(string login)
        {
            var user = GetUser(login);
            StringBuilder builder = new StringBuilder();
            GetUserAccounts(user).ToList().ForEach(s=> builder.AppendLine(s.GetAccountInfo()));
            return builder.ToString();
        }
        public String GetAccountInfoWithHistory(string login)
        {
            var user = GetUser(login);
            StringBuilder builder = new StringBuilder();
            GetUserAccounts(user).ToList().ForEach(x => 
            {
                builder.Append(x.GetAccountInfo());
                GetAccountHistories(x).ToList().ForEach(d => builder.Append(d.GetHistoryInfo()));
            });
            return builder.ToString();
        }

        private IEnumerable<Account> GetUserAccounts(User user)
        {
            if (!IsUserExist(user.Login)) return Enumerable.Empty<Account>();
            return manager.Accounts.Where(x => x.UserId == user.Id);
        }
        public IEnumerable<OperationsHistory> GetAccountHistories(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            return manager.History.Where(x => account.Id == x.AccountId);
        }
        public string GetInfoAboutInputCashOperations()
        {
            StringBuilder builder = new StringBuilder();
            var query = from h in manager.History
                        where h.OperationType == OperationType.InputCash
                        join a in manager.Accounts on h.AccountId equals a.Id
                        join u in manager.Users on a.UserId equals u.Id
                        select new {Id = h.Id, Type = h.OperationType, Data = h.OperationDate, Sum = h.CashSum,  User = u.GetUserInfo()};
            foreach (var q in query)
            {
                builder.AppendLine($"Operation ID: {q.Id}");
                builder.AppendLine($"Operation Type: {q.Type}");
                builder.AppendLine($"Operation Date: {q.Data}");
                builder.AppendLine($"Sum: {q.Sum}");
                builder.AppendLine($"User:\n{q.User}");
                builder.Append("===================================\n");
            }
            return builder.ToString(); ;
        }
        public string GetUserSumm(decimal N)
        {
            StringBuilder builder = new StringBuilder();

            var query = from a in manager.Accounts
                .GroupBy(s => s.UserId)
                .Select(x => new { Id = x.Key, Sum = x.Sum(t => t.CashAll) })
                .Where(n => n.Sum >= N)
                        join u in manager.Users on a.Id equals u.Id
                        select new { User = u, Sum = a.Sum};

            foreach (var q in query)
            {
                builder.AppendLine($"User:\n{q.User.GetUserInfo()}");
                builder.AppendLine($"Sum: {q.Sum}");
                builder.Append("===================================\n");
            }
            return builder.ToString();
        }
    }
}
