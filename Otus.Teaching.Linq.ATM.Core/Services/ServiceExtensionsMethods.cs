﻿using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services.Extensions
{
    internal static class ServiceExtensionsMethods
    {
        public static string GetUserInfo(this User user)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"First Name: {user.FirstName}");
            builder.AppendLine($"Middle Name: {user.MiddleName}");
            builder.AppendLine($"Last Name: {user.SurName}");
            builder.AppendLine($"Passport: {user.PassportSeriesAndNumber}");
            builder.AppendLine($"Phone: {user.Phone}");
            builder.AppendLine($"Registration: {user.RegistrationDate}");

            return builder.ToString();
        }

        public static string GetAccountInfo(this Account account)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"Id: {account.Id}");
            builder.AppendLine($"CashAll: {account.CashAll}");
            builder.AppendLine($"Openning date: {account.OpeningDate}");
            return builder.ToString();
        }
        
        public static string GetHistoryInfo(this OperationsHistory history)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"Cash summ: {history.CashSum}");
            builder.AppendLine($"Operation date: {history.OperationDate}");
            builder.AppendLine($"Operation type: {history.OperationType}");

            return builder.ToString();
        }
        public static bool CheckPassword(this User user, string Password) => user.Password.Equals(Password);
    }
}
